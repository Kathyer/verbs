import { Injectable } from '@angular/core';
import { Pregunta } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class VerbosService
{
	verbos = [
		{v: "Back up", s: "Apoyar"},
		{v: "Be up", s: "Tramar algo"},
		{v: "Be up", s: "Es asunto de alguien"},
		{v: "Break down", s: "Romper algo"},
		{v: "Break in/into", s: "Entrar forzando una puerta"},
		{v: "Break up", s: "Romper una relación"},
		{v: "Bring up", s: "Educar"},
		{v: "Bring up", s: "Sacar un tema"},
		{v: "Build up", s: "Fortalecer"},
		{v: "Call up", s: "Llamar por teléfono"},
		{v: "Call off", s: "Cancelar"},
		{v: "Calm down", s: "Calamar"},
		{v: "Carry on", s: "Continuar"},
		{v: "Carry out", s: "Llevar a cabo algo"},
		{v: "Catch up", s: "Ponerse al día"},
		{v: "Check in", s: "Registrarse en un hotel"},
		{v: "Check out", s: "Pagar la cuenta o carrito"},
		{v: "Cheer up", s: "Animar o alegrar"},
		{v: "Clear up", s: "Aclararse"},
		{v: "Clear up", s: "Limpiar"},
		{v: "Come Across", s: "Encontrarse con alguien"},
		{v: "Come With", s: "Encontrar una solución"},
		{v: "Cut off", s: "Desconectar"},
		{v: "Cut down on", s: "Reducir algo (gastos, peso, etc)"},
		{v: "Dress up", s: "Vestir elegante"},
		{v: "Dress up", s: "Disfrazarse"},
		{v: "Eat in", s: "Comer dentro de casa"},
		{v: "Eat out", s: "Comer fuera de casa"},
		{v: "End up", s: "Acabar"},
		{v: "Fall out", s: "Discutir con alguien"},
		{v: "Fall out", s: "Enfadarse con alguien"},
		{v: "Get somebody down", s: "Deprimirse"},
		{v: "Get over", s: "Sobreponerse"},
		{v: "Give up", s: "Rendirse"},
		{v: "Go down", s: "Ofenderse"},
		{v: "Go down", s: "Descender"},
		{v: "Go down", s: "Tener una acogida"},
		{v: "Go on", s: "Continuar"},
		{v: "Go off", s: "Explotar"},
		{v: "Go off", s: "Irse sin avisar"},
		{v: "Keep on", s: "Continuar"},
		{v: "Look out", s: "Tener cuidado con alguien"},
		{v: "Look forward to", s: "Tener muchas ganas de algo"},
		{v: "Look after", s: "Cuidar a alguien"},
		{v: "Look for", s: "Buscar"},
		{v: "Look into", s: "Estudiar algo"},
		{v: "Look into", s: "Investigar"},
		{v: "Look up", s: "Buscar en el diccionario"},
		{v: "Look on", s: "Considerar"},
		{v: "Make up", s: "Maquillarse"},
		{v: "Make up", s: "Inventar"},
		{v: "Make up with", s: "Hacer las paces"},
		{v: "Make up with", s: "Reconciliarse"},
		{v: "Pick up", s: "Recoger a alguien"},
		{v: "Put down", s: "Humillar"},
		{v: "Put down", s: "Sacrificar animales"},
		{v: "Put across", s: "Transmitir"},
		{v: "Put off", s: "Posponer"},
		{v: "Set up", s: "Montar un negocio"},
		{v: "Set in", s: "Comenzar"},
		{v: "Take after", s: "Parecerse a alguien"},
		{v: "Take back", s: "Devolver algo"},
		{v: "Take off", s: "Despegar"},
		{v: "Take over", s: "Tener el control"},
		{v: "Take up", s: "Participar en algo"},
		{v: "Take care", s: "Cuidar"},
		{v: "Take in", s: "Engañar"},
		{v: "Take in", s: "Asimilar"},
		{v: "Take out", s: "Hacer ejercicio"},
		{v: "Walk out", s: "Abandonar"},
		{v: "Take someone for", s: "Confundir a alguien"},
		{v: "Take someone for", s: "Dar por supuesto"},
		{v: "Take someone for", s: "Subestimar"},
	];

	public verbosResumido = []

	public aciertos = 0;
	public fallos = 0;
	public ignorados = 0;
	public porcentajeAciertos = "0";

	constructor()
	{
		// Creamos la lista de verbos resumidos, es decir, añadirle mas de un significado en caso de que tenga
		this.verbosResumido = [];

		this.aciertos = 0;
		this.fallos = 0;
		this.ignorados = 0;

		let verboAnterior = this.verbos[0].v;
		let s = [this.verbos[0].s];

		for (let i = 1 ; i < this.verbos.length; i++)
		{
			if (verboAnterior == this.verbos[i].v)
			{
				s.push(this.verbos[i].s)
			}
			else
			{
				this.verbosResumido.push({v: verboAnterior, s: s});

				verboAnterior = this.verbos[i].v;
				s = [this.verbos[i].s];
			}
		}

		// Último
		this.verbosResumido.push({v: verboAnterior, s: s});
	}

	public aumentarAciertos()
	{
		this.aciertos++;
		this.recalcularPorcentajeAciertos();
	}

	public aumentarFallos()
	{
		this.fallos++;
		this.recalcularPorcentajeAciertos();
	}

	public aumentarIgnorados()
	{
		this.ignorados++;
		this.recalcularPorcentajeAciertos();
	}

	private recalcularPorcentajeAciertos()
	{
		this.porcentajeAciertos = (this.aciertos / (this.aciertos + this.fallos + this.ignorados) * 100).toFixed(0);
	}

	public obtenerPregunta(): Pregunta
	{
		let pregunta: Pregunta = {
			pregunta: "",
			respuestas: [],
			correcta: "",
			tipo: Math.floor(Math.random() * 2)
		};

		let indiceVerboSeleccionado = Math.floor(Math.random() * this.verbos.length + 1);
		let verboSeleccionado = {v: this.verbos[indiceVerboSeleccionado].v, s: this.verbos[indiceVerboSeleccionado].s};
		let respuestas = [];

		// Ahora, obtenemos las respuestas, las cuales deben ser un verbo diferente al de la pregunta

		for (let i = 1; i < 4; i++)
		{
			let nVerboAleatorio = Math.floor(Math.random() * this.verbos.length);

			while (this.verbos[nVerboAleatorio].v == verboSeleccionado.v || this.verbos[nVerboAleatorio].s == verboSeleccionado.s)
				nVerboAleatorio = Math.floor(Math.random() * this.verbos.length + 1);

			respuestas.push({v: this.verbos[nVerboAleatorio].v, s: this.verbos[nVerboAleatorio].s});
		}

		// Ahora, rellenamos el valor de la pregunta a devolver según el tipo
		// Tipo: 0 es preguntar el verbo en inglés y responder en español. 1 Es pregunta es verbo en español y respuestas en inglés
		if (pregunta.tipo == 0)
		{
			pregunta.pregunta = verboSeleccionado.v;
			pregunta.respuestas[0] = verboSeleccionado.s;
			pregunta.respuestas[1] = respuestas[0].s;
			pregunta.respuestas[2] = respuestas[1].s;
			pregunta.respuestas[3] = respuestas[2].s;
			pregunta.correcta = verboSeleccionado.s;
		}
		else
		{
			pregunta.pregunta = verboSeleccionado.s;
			pregunta.respuestas[0] = verboSeleccionado.v;
			pregunta.respuestas[1] = respuestas[0].v;
			pregunta.respuestas[2] = respuestas[1].v;
			pregunta.respuestas[3] = respuestas[2].v;
			pregunta.correcta = verboSeleccionado.v;
		}

		// Ahora descolocamos las respuestas para que sean aleatorias
		// let respuestasBackup = [...pregunta.respuestas];
		let respuestasBackup = [];

		// Hago esto para evitar que trabaje por referencia y lo haga por copia
		respuestasBackup[0] = pregunta.respuestas[0];
		respuestasBackup[1] = pregunta.respuestas[1];
		respuestasBackup[2] = pregunta.respuestas[2];
		respuestasBackup[3] = pregunta.respuestas[3];
		let i = 0;

		while (respuestasBackup.length != 0)
		{
			// Obtenemos una respuesta aleatoria
			let respuestaAleatoria = Math.floor(Math.random() * respuestasBackup.length);

			// La guardamos en la posición i
			pregunta.respuestas[i] = respuestasBackup[respuestaAleatoria];

			// Borramos la seleccionada
			respuestasBackup.splice(respuestaAleatoria, 1);

			// Incrementamos i
			i++;
		}

		return pregunta;
	}
}
