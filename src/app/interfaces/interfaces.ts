import { NumericValueAccessor } from '@ionic/angular';

export interface Pregunta
{
	pregunta: string; // Pregunta, que puede ser un verbo o un significado en español
	respuestas: string[]; // Array con las respuestas
	correcta: string; // Cadena con la respuesta correcta
	tipo: number; // Tipo: 0 es preguntar el verbo en inglés y responder en español. 1 Es pregunta es verbo en español y respuestas en inglés
}