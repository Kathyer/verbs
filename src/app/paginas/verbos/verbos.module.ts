import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { VerbosPage } from './verbos.page';

import { VerbosPageRoutingModule } from './verbos-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    VerbosPageRoutingModule
  ],
  declarations: [VerbosPage]
})
export class VerbosPageModule {}
