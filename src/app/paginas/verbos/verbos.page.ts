import { Component } from '@angular/core';
import { VerbosService } from '../../servicios/verbos.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'verbos.page.html',
  styleUrls: ['verbos.page.scss']
})
export class VerbosPage
{
	constructor(public verbosCtrl: VerbosService) {}
}
