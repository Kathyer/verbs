import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VerbosPage } from './verbos.page';

const routes: Routes = [
  {
    path: '',
    component: VerbosPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerbosPageRoutingModule {}
