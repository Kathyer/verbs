import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'preguntas',
        loadChildren: () => import('../preguntas/preguntas.module').then(m => m.PreguntasPageModule)
      },
      {
        path: 'verbs',
        loadChildren: () => import('../verbos/verbos.module').then(m => m.VerbosPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/preguntas',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/preguntas',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
