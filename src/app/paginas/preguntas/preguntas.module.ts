import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PreguntasPage } from './preguntas.page';

import { PreguntasPageRoutingModule } from './preguntas-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    PreguntasPageRoutingModule
  ],
  declarations: [PreguntasPage]
})
export class PreguntasPageModule {}
