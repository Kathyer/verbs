import { Component } from '@angular/core';
import { Pregunta } from 'src/app/interfaces/interfaces';
import { VerbosService } from 'src/app/servicios/verbos.service';

@Component({
  selector: 'app-preguntas',
  templateUrl: 'preguntas.page.html',
  styleUrls: ['preguntas.page.scss']
})
export class PreguntasPage
{
	respuestasClases:string[];
	sePuedeResponder:boolean;
	pregunta: Pregunta;
	porcentajeAciertos: number = 0;

	constructor(public verbosCtrl: VerbosService)
	{
		this.pregunta = null;

		this.nuevaPregunta();
		this.sePuedeResponder = true;
	}

	public botonRespuesta()
	{
		// Si se puede responder (aún no se ha respondido), entonces hay que marcar como pregunta errónea al marcar cual será la respuesta
		if(this.sePuedeResponder == true)
		{
			this.sePuedeResponder = false;
			this.verbosCtrl.aumentarFallos();

			// Marcamos la correcta en verde
			for (let i = 0; i < 4; i++)
			{
				if (this.pregunta.respuestas[i] == this.pregunta.correcta)
					this.respuestasClases[i] = "respuetaCorrecta";
			}
		}
	}

	public botonSiguiente()
	{
		// Si se puede responder, entonces hay que marcar como pregunta ignorada (aún no se ha respondido)
		if(this.sePuedeResponder == true)
		{
			this.verbosCtrl.aumentarIgnorados();
		}

		this.nuevaPregunta();
	}

	private inicializarRespuestas()
	{
		this.respuestasClases = [];

		for (let i = 0; i < 4; i++)
			this.respuestasClases[i] = "tarjetaRespuesta";
	}

	private nuevaPregunta()
	{
		this.inicializarRespuestas();

		this.pregunta = <Pregunta> this.verbosCtrl.obtenerPregunta();

		this.sePuedeResponder = true;
	}

	public responder(respuesta)
	{
		if (this.sePuedeResponder)
		{
			this.sePuedeResponder = false;

			if(this.pregunta.respuestas[respuesta] == this.pregunta.correcta)
			{
				this.respuestasClases[respuesta] = "respuetaCorrecta";
				this.verbosCtrl.aumentarAciertos();
			}
			else
			{
				this.respuestasClases[respuesta] = "respuestaErronea";
				this.verbosCtrl.aumentarFallos();

				// Marcamos la correcta en verde
				for (let i = 0; i < 4; i++)
				{
					if (this.pregunta.respuestas[i] == this.pregunta.correcta)
						this.respuestasClases[i] = "respuetaCorrecta";
				}
			}
		}
	}
}
